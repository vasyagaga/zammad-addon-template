# zammad-addon-template

Bootstrap your zammad addon.

## Development

#### One time setup

1. Run `make init`
   Example:
   ```console
   $ make init
   Give your addon a name. No spaces.
   Addon name?:
   test
   ```
2. `rm -rf .git`
3. `git init`
4. `git add . && git commit -m "Initial commit"`

### Create your addon
1. Edit the files in `src/`

   Migration files should go in `src/db/addon/<NAME>` ([see this post](https://community.zammad.org/t/automating-creation-of-custom-object-attributes/3831/2?u=abelxluck))

2. Update version and changelog in `<NAME>-skeleton.szpm`
3. Build a new package `make`

   This outputs `dist/<NAME>-vXXX.szpm`

4. Install the szpm using the zammad package manager.

5. Repeat


### Create a new migration

Included is a helper script to create new migrations. You must have the python
`inflection` library installed.

* debian/ubuntu:  `apt install python3-inflection`
* pip: `pip install --user inflection`
* or create your own venv

To make a new migration simply run:
```
make new-migration
```

## License

[![License GNU AGPL v3.0](https://img.shields.io/badge/License-AGPL%203.0-lightgrey.svg)](https://gitlab.com/digiresilience/link/zamamd-addon-template/blob/master/LICENSE.md)

This is a free software project licensed under the GNU Affero General
Public License v3.0 (GNU AGPLv3) by [The Center for Digital
Resilience](https://digiresilience.org) and [Guardian
Project](https://guardianproject.info).


🤠
